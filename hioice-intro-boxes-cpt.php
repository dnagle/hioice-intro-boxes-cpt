<?php
/**
 * Plugin Name: HIOICE Intro Boxes Custom Post Type
 * Description: Adds a Custom Post Type for the 'Maxmedia 2017' theme, used in the 'How We Work' section. 
 * Author: Dan Nagle
 * Text Domain: hioice-intro-boxes
 * Domain Path: /languages/
 * Version: 1.0.3
 * License: GPLv2
 * Bitbucket Plugin URI: https://bitbucket.org/dnagle/hioice-intro-boxes-cpt
 *
 * @package WordPress
 * @author Dan Nagle
 */

if ( !defined( 'HIOICE_IB_VERSION' ) ) {
	define( 'HIOICE_IB_VERSION', '1.0.0' ); // Plugin version
}
if ( !defined( 'HIOICE_IB_DIR' ) ) {
	define( 'HIOICE_IB_DIR', dirname( __FILE__ ) ); // Plugin directory
}
if ( !defined( 'HIOICE_IB_URL' ) ) {
	define( 'HIOICE_IB_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}
if ( !defined( 'HIOICE_IB_DOMAIN' ) ) {
	define( 'HIOICE_IB_DOMAIN', 'hioice-intro-boxes' ); // Text Domain
}

/**
 * Load Text Domain
 * This gets the plugin ready for translation
 * 
 * @package Intro Boxes
 * @since 1.0.0
 */
function hioice_ib_load_textdomain() {
	load_plugin_textdomain( HIOICE_IB_DOMAIN, false, HIOICE_IB_DIR . '/languages/' );
}
add_action('plugins_loaded', 'hioice_ib_load_textdomain');


function hioice_create_intro_boxes_post_type() {

	register_post_type( 'intro_boxes',
		array(
			'labels' => array(
				'name' => __( 'Intro Boxes', HIOICE_IB_DOMAIN ),
				'singular_name' => __( 'Intro Box', HIOICE_IB_DOMAIN ),
				'add_new' => __( 'Add New', HIOICE_IB_DOMAIN ),
				'add_new_item' => __( 'Add New Intro Box', HIOICE_IB_DOMAIN ),
				'edit' => __( 'Edit', HIOICE_IB_DOMAIN ),
				'edit_item' => __( 'Edit Intro Box', HIOICE_IB_DOMAIN ),
				'new_item' => __( 'New Intro Box', HIOICE_IB_DOMAIN ),
				'view' => __( 'View', HIOICE_IB_DOMAIN ),
				'view_item' => __( 'View Intro Box', HIOICE_IB_DOMAIN ),
				'search_items' => __( 'Search Intro Boxes', HIOICE_IB_DOMAIN ),
				'not_found' => __( 'No Intro Boxes found', HIOICE_IB_DOMAIN ),
				'not_found_in_trash' => __( 'No Intro Boxes found in Trash', HIOICE_IB_DOMAIN ),
				'parent' => __( 'Parent Intro Box', HIOICE_IB_DOMAIN ),
			),
			'public' => false,
			'show_ui' => true,
			'menu_position' => 20,
			'supports' => array(
				'title', 'editor', 'thumbnail'
			),
			'taxonomies' => array( '' ),
			'menu_icon' => plugins_url( 'images/intro-boxes-icon.png', __FILE__ ),
			'has_archive' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
		)
	);

}
add_action( 'init', 'hioice_create_intro_boxes_post_type' );


function hioice_intro_boxes_shortcode( $atts, $content=null ) {

	$defaults = array( 'post_ids' => '-1' );

	$output = '';
	
	extract( shortcode_atts($defaults, $atts) );

	$query_params = array(
		'post_type' => 'intro_boxes',
		'post_status' => 'published',
 		'ignore_sticky_posts' => 1,
		'orderby' => 'ID',
		'order' => 'ASC',
		'suppress_filters' => true,
	);

	$box_ids = explode( ',', $post_ids );

	if ( count($box_ids) > 0 && $box_ids[0] > 0 ) {
		$query_params['post__in'] = $box_ids;
	}

	$box_query = new WP_Query( $query_params );

	$odd_or_even = 'even';

	if ( $box_query->have_posts() ) {

		ob_start();
?>
<div class="intro-box-container">
<?php
		while ( $box_query->have_posts() ) {

			$box_post = $box_query->the_post();

			$odd_or_even = ($odd_or_even == 'odd' ? 'even' : 'odd');
?>
	<div class="intro-box-wrap <?php echo $odd_or_even; ?>">
		<div class="intro-box-img"><?php echo get_the_post_thumbnail( get_the_ID() ); ?></div>
		<div class="intro-box-content <?php echo $odd_or_even; ?>">
			<h3><?php echo get_the_title( get_the_ID() ); ?></h3>
			<p><?php echo get_the_content( get_the_ID() ); ?></p>
		</div>
	</div>
<?php
		}

		wp_reset_postdata();
?>
</div>
<?php
		$output = ob_get_contents();
		ob_end_clean();
	}

	return $output;
}


/*
 * Function for registering the [intro-boxes] shortcode.
 */
function hioice_ib_register_shortcodes() {
	add_shortcode( 'hioice-intro-boxes', 'hioice_intro_boxes_shortcode' );
}
/* Register shortcodes in 'init'. */
add_action( 'init', 'hioice_ib_register_shortcodes' );
